# README #

### Summary of ByParts ###

This package extends xTensor by providing an easy interface for performing integrations by parts by moving derivatives, assuming that boundary terms may be neglected. 

Version 1.0

Copyright (C) 2013-2014, University of Sussex, under the General Public Licence

### Getting the repository working ###

**Dependencies**

1) Mathematica.

2) xAct (freely available at http://xact.es, along with installation instructions)

**Installation**

1) If not already done, check that you can run the default xAct package example files.

2) Download the repository contents (one directory and one linker file) and place them at the top level within the xAct directory, wherever it is on your computer. 

3) Open the examples file and run it. If it runs, the installation worked.

### Contribution guidelines ###

Contributions welcomed!

Please ensure that changes are clear and explain why the change is needed.

Before making substantial changes it would probably be advisable to email the author first in case there is some subtle reason why your change has not already been made!

### Contact ###

Author: Joseph Elliston. 

Please email j.elliston@sussex.ac.uk with bugs/fixes/questions/comments.